using Caissa.Repository.Implementation;
using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Caissa.Repository.Test
{
    [TestClass]
    public class PriceRepositoryTest
    {                
        private IPriceRepository repository;
        private Price price;

        [TestInitialize]
        public void Init()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var provider = (Microsoft.Extensions.Configuration.Json.JsonConfigurationProvider)config.Providers.First();

            provider.TryGet("CaissaDatabase", out var value);

            repository = new PriceRepository(value);
        }

        [TestMethod]
        public void Should_Get()
        {
            var result = repository.GetBySecurityId(1, 0, 5);
            result = repository.GetBySecurityId(1, 1, 5);

            Assert.IsTrue(result.Total > 0);
        }

        [TestMethod]
        public void Should_Insert()
        {
            price = GetSamplePrice();
            price = repository.Add(price);

            Assert.IsTrue(price.PriceId > 0);
        }

        [TestMethod]
        public void Should_Delete()
        {
            price = GetSamplePrice();
            price = repository.Add(price);

            var result = repository.Delete(price.PriceId);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Should_Edit()
        {
            price = GetSamplePrice();
            price.PriceId = 1;
            price.EODPrice = 12.34;
            price = repository.Edit(price);

            Assert.IsTrue(price.EODPrice == 12.34);
        }

        private Price GetSamplePrice()
        {
            return new Price()
            {
                Date = DateTime.Now,
                SecurityId = 1,
                EODPrice = 56.89
            };
        }
    }
}
