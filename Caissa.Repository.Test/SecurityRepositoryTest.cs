using Caissa.Repository.Implementation;
using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Caissa.Repository.Test
{
    [TestClass]
    public class SecurityRepositoryTest
    {                
        private ISecurityRepository repository;
        private Security security;

        [TestInitialize]
        public void Init()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var provider = (Microsoft.Extensions.Configuration.Json.JsonConfigurationProvider)config.Providers.First();

            provider.TryGet("CaissaDatabase", out var value);

            repository = new SecurityRepository(value);
        }

        [TestMethod]
        public void Should_Get()
        {
            var result = repository.Get(0, 5);
            result = repository.Get(1, 5);

            Assert.IsTrue(result.Total > 0);
        }

        [TestMethod]
        public void Should_Insert()
        {
            security = GetSampleSecurity();
            security = repository.Add(security);

            Assert.IsTrue(security.SecurityId > 0);
        }

        [TestMethod]
        public void Should_Delete()
        {
            security = GetSampleSecurity();
            security = repository.Add(security);

            var result = repository.Delete(security.SecurityId);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Should_Edit()
        {
            security = GetSampleSecurity();
            security.SecurityId = 1;
            security.Country = "UK";
            security = repository.Edit(security);

            Assert.IsTrue(security.Country == "UK");
        }

        private Security GetSampleSecurity()
        {
            return new Security()
            {
                Name = "Test Security",
                ISIN = "Test ISIN",
                Country = "USA"
            };
        }
    }
}
