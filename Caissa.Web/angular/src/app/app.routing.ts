import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppMainComponent } from "./component/app.main";
import { AppSecuritiesComponent } from "./component/app.securities";
import { AppPricesComponent } from "./component/app.prices";

export const AppPaths: Routes = [
    { path: "", redirectTo: "securities", pathMatch: "full" },
    { path: "securities", component: AppSecuritiesComponent },
    { path: "securities/add", component: AppSecuritiesComponent },
    { path: "securities/edit/:id", component: AppSecuritiesComponent },
    { path: "securities/delete/:id", component: AppSecuritiesComponent },
    { path: "securities/:id/prices", component: AppPricesComponent },
    { path: "securities/:id/prices/add", component: AppPricesComponent },
    { path: "securities/:id/prices/edit/:pid", component: AppPricesComponent },
    { path: "securities/:id/prices/delete/:pid", component: AppPricesComponent },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppPaths);
