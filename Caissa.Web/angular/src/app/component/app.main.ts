import { Component } from "@angular/core";
import { AppBaseComponent } from "./app.base";
import { UiService } from "../service/uiService";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: "app-main",
    templateUrl: "../template/main.html"
})

export class AppMainComponent extends AppBaseComponent {
    constructor(public uiService: UiService, protected router: Router, protected route: ActivatedRoute) {
        super(uiService, router, route);
    }
}
