import { UiService } from "../service/uiService";
import { Router, ActivatedRoute } from "@angular/router";
import { ViewMode } from "../model/viewMode";
import { Path } from "../model/path";
import { PageRequest } from "../model/pageRequest";
import { environment } from "../../environments/environment.prod";

export class AppBaseComponent {
    protected url = this.route.url as any;
    public error: string;
    public viewMode: ViewMode = ViewMode.List;
    protected sub: any;
    protected path: Path[] = [];
    public pageRequest: PageRequest = new PageRequest();

    constructor(public uiService: UiService, protected router: Router, protected route: ActivatedRoute) {
        this.pageRequest.currentPage = 0;
        this.pageRequest.pageSize = environment.pageSize;
    }

    protected setPath(): void {
        setTimeout(() => {
            this.uiService.path = this.path;
        }, 100);
    }

    public update(pageRequest: PageRequest): void {
        this.pageRequest = pageRequest;
        this.getData();
    }

    protected getData(): void {

    }
}
