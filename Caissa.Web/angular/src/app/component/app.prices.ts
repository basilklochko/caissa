import { Component, OnInit, OnDestroy } from "@angular/core";
import { SecurityService } from "../service/securityService";
import { Security } from "../model/security";
import { ViewMode } from "../model/viewMode";
import { AppBaseComponent } from "./app.base";
import { UiService } from "../service/uiService";
import { Router, ActivatedRoute } from "@angular/router";
import { Path } from "../model/path";
import { Price } from "../model/price";
import { PriceService } from "../service/priceService";

@Component({
    selector: "app-prices",
    templateUrl: "../template/prices.html"
})

export class AppPricesComponent extends AppBaseComponent implements OnInit, OnDestroy {
    public security: Security = new Security();
    public prices: Price[];
    public price: Price = new Price();

    constructor(public uiService: UiService, protected router: Router, protected route: ActivatedRoute, private securityService: SecurityService, private priceService: PriceService) {
        super(uiService, router, route);
    }

    ngOnInit(): void {
        const root = new Path();
        root.title = "Securities";
        root.url = "/";
        this.path.push(root);

        const sub = new Path();

        this.sub = this.route.params.subscribe(params => {
            if (params["id"] !== undefined && params["id"] != null) {
                this.security.securityId = params["id"];

                this.securityService.GetOne(this.security).subscribe((res: any) => {
                    this.security = res.data as Security;

                    if (this.security.securityId === 0) {
                        this.router.navigate(["/securities"]);
                    }

                    this.price.securityId = this.security.securityId;

                    const subRoot = new Path();
                    subRoot.title = this.security.name;
                    subRoot.url = "/securities/edit/" + this.security.securityId;
                    this.path.push(subRoot);

                    const subSubRoot = new Path();
                    subSubRoot.title = "Prices";
                    subSubRoot.url = "/securities/" + this.security.securityId + "/prices";
                    this.path.push(subSubRoot);

                    if (this.url.value[this.url.value.length - 1].path === "add") {
                        this.viewMode = ViewMode.Add;

                        this.sub.title = "Add";
                        this.path.push(this.sub);
                    } else if (this.url.value.length > 1 && (this.url.value[this.url.value.length - 2].path === "edit" || this.url.value[this.url.value.length - 2].path === "delete")) {
                        if (params["pid"] !== undefined && params["pid"] != null) {
                            this.price.priceId = params["pid"];

                            this.priceService.GetOne(this.price).subscribe((r: any) => {
                                this.price = r.data as Price;

                                if (this.price.priceId === 0) {
                                    this.router.navigate(["/securities/" + this.price.securityId + "/prices"]);
                                }

                                this.viewMode = this.url.value[this.url.value.length - 2].path === "edit" ? ViewMode.Edit : ViewMode.Delete;
                                sub.title = this.url.value[this.url.value.length - 2].path === "edit" ? "Edit" : "Delete";

                                this.path.push(sub);
                            });
                        }
                    } else {
                        this.getData();
                    }
                });
            }
        });

        this.setPath();
    }

    ngOnDestroy() {
        if (this.sub !== undefined) {
            this.sub.unsubscribe();
        }
    }

    public isValid(): boolean {
        let result = true;

        if (!this.price || Object.keys(this.price).length === 0) {
            result = false;
        } else {
            if (Object.getOwnPropertyNames(this.price).filter(p => p !== "priceId").length < 3) {
                result = false;
            } else {
                for (const key of Object.keys(this.price)) {
                    if (!this.price[key] || this.price[key].length === 0) {
                        result = false;
                        break;
                    }
                }
            }
        }

        return result;
    }

    public onConfirm(): void {
        this.uiService.loading();

        this.priceService.Delete(this.price).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }

            this.onCancel();
        });
    }

    public onSave(): void {
        this.uiService.loading();

        this.priceService.Save(this.price).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }

            this.onCancel();
        });
    }

    public onAdd(): void {
        this.router.navigate(["/securities/" + this.security.securityId + "/prices/add"]);
    }

    public onEdit(id: number): void {
        this.router.navigate(["/securities/" + this.security.securityId + "/prices/edit/" + id]);
    }

    public onDelete(id: number): void {
        this.router.navigate(["/securities/" + this.security.securityId + "/prices/delete/" + id]);
    }

    public onCancel(): void {
        this.router.navigate(["/securities/" + this.security.securityId + "/prices"]);
    }

    protected getData(): void {
        this.uiService.loading();

        this.priceService.GetAll(this.security, this.pageRequest).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }

            this.pageRequest.total = res.total;
            this.prices = res.data;
        });
    }
}
