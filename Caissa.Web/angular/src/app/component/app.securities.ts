import { Component, OnInit, OnDestroy } from "@angular/core";
import { SecurityService } from "../service/securityService";
import { Security } from "../model/security";
import { ViewMode } from "../model/viewMode";
import { AppBaseComponent } from "./app.base";
import { UiService } from "../service/uiService";
import { Router, ActivatedRoute } from "@angular/router";
import { Path } from "../model/path";
import { PageRequest } from "../model/pageRequest";

@Component({
    selector: "app-securities",
    templateUrl: "../template/securities.html"
})

export class AppSecuritiesComponent extends AppBaseComponent implements OnInit, OnDestroy {
    public securities: Security[];
    public security: Security = new Security();

    constructor(public uiService: UiService, protected router: Router, protected route: ActivatedRoute, private securityService: SecurityService) {
        super(uiService, router, route);
    }

    ngOnInit(): void {
        const root = new Path();
        root.title = "Securities";
        root.url = "/";
        this.path.push(root);

        const sub = new Path();

        if (this.url.value[this.url.value.length - 1].path === "add") {
            this.viewMode = ViewMode.Add;

            sub.title = "Add";
            this.path.push(sub);
        } else if (this.url.value.length > 1 && (this.url.value[1].path === "edit" || this.url.value[1].path === "delete")) {
            this.sub = this.route.params.subscribe(params => {
                if (params["id"] !== undefined && params["id"] != null) {
                    this.security.securityId = params["id"];

                    this.securityService.GetOne(this.security).subscribe((res: any) => {
                        this.security = res.data as Security;

                        if (this.security.securityId === 0) {
                            this.onCancel();
                        } else {
                            this.viewMode = this.url.value[1].path === "edit" ? ViewMode.Edit : ViewMode.Delete;
                            sub.title = this.url.value[1].path === "edit" ? "Edit" : "Delete";
                        }
                    });

                    this.path.push(sub);
                }
            });
        } else {
            this.getData();
        }

        this.setPath();
    }

    ngOnDestroy() {
        if (this.sub !== undefined) {
            this.sub.unsubscribe();
        }
    }

    public isValid(): boolean {
        let result = true;

        if (!this.security || Object.keys(this.security).length === 0) {
            result = false;
        } else {
            if (Object.getOwnPropertyNames(this.security).filter(p => p !== "securityId").length < 3) {
                result = false;
            } else {
                for (const key of Object.keys(this.security)) {
                    if (!this.security[key] || this.security[key].length === 0) {
                        result = false;
                        break;
                    }
                }
            }
        }

        return result;
    }

    public onPrices(id: number): void {
        this.security = this.securities.filter(s => s.securityId === id)[0];

        this.router.navigate(["/securities/" + this.security.securityId + "/prices"]);
    }

    public onConfirm(): void {
        this.uiService.loading();

        this.securityService.Delete(this.security).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }

            this.onCancel();
        });
    }

    public onSave(): void {
        this.uiService.loading();

        this.securityService.Save(this.security).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }

            this.onCancel();
        });
    }

    public onAdd(): void {
        this.router.navigate(["/securities/add"]);
    }

    public onEdit(id: number): void {
        this.router.navigate(["/securities/edit/" + id]);
    }

    public onDelete(id: number): void {
        this.router.navigate(["/securities/delete/" + id]);
    }

    public onCancel(): void {
        this.router.navigate(["/securities"]);
    }

    protected getData(): void {
        this.uiService.loading();

        this.securityService.GetAll(this.pageRequest).subscribe((res: any) => {
            this.uiService.loaded();

            if (res.error) {
                this.error = res.error;
                return;
            }
            this.pageRequest.total = res.total;
            this.securities = res.data;
        });
    }
}
