import { Component, Input, EventEmitter, Output } from "@angular/core";
import { AppBaseComponent } from "./app.base";
import { UiService } from "../service/uiService";
import { Router, ActivatedRoute } from "@angular/router";
import { PageRequest } from "../model/pageRequest";

@Component({
    selector: "app-pager",
    templateUrl: "../template/pager.html"
})

export class AppPagerComponent extends AppBaseComponent {
    @Input() pageRequest: PageRequest;
    @Output() updateEvent: EventEmitter<any> = new EventEmitter();

    constructor(public uiService: UiService, protected router: Router, protected route: ActivatedRoute) {
        super(uiService, router, route);
    }

    public getPagesCount(): number {
        return Math.ceil(this.pageRequest.total / this.pageRequest.pageSize);
    }

    public navigate(dir: any): void {
        switch (dir) {
            case 0:
                if (this.pageRequest.currentPage === 0) {
                    return;
                }

                this.pageRequest.currentPage = dir;
                break;

            case -1:
                if (this.pageRequest.currentPage === 0) {
                    return;
                }

                this.pageRequest.currentPage--;
                break;

            case 1:
                if (this.pageRequest.currentPage + 1 === this.getPagesCount()) {
                    return;
                }

                this.pageRequest.currentPage++;
                break;

            default:
                if (this.pageRequest.currentPage + 1 === this.getPagesCount()) {
                    return;
                }

                this.pageRequest.currentPage = this.getPagesCount() - 1;
                break;
        }

        this.updateEvent.emit(this.pageRequest);
    }
}
