import { Injectable } from "@angular/core";
import { Path } from "../model/path";

@Injectable()
export class UiService {
    public isLoading = false;
    public path: Path[] = [];

    public loading(): void {
        const self = this;

        setTimeout(function () {
            self.isLoading = true;
        }, 100);
    }

    public loaded(): void {
        const self = this;

        setTimeout(function () {
            self.isLoading = false;
        }, 100);
    }
}
