import { Injectable } from "@angular/core";
import { Http, Response, RequestOptions, Headers } from "@angular/http";
// tslint:disable-next-line:import-blacklist
import { Observable } from "rxjs/Rx";
import { BaseService } from "./baseService";
import { Price } from "../model/price";
import { Security } from "../model/security";
import { PageRequest } from "../model/pageRequest";

@Injectable()
export class PriceService extends BaseService {
    constructor(protected http: Http) {
        super(http);
    }

    public GetOne(price: Price): Observable<Price> {
        return this.http.get(this.apiUrl + "prices/get-one/" + price.priceId, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }

    public GetAll(security: Security, request: PageRequest): Observable<any> {
        return this.http.post(this.apiUrl + "prices/get-all/" + security.securityId, request, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }

    public Save(price: Price): Observable<any> {
        if (price.priceId) {
            return this.http.put(this.apiUrl + "prices/" + price.priceId, price, this.createAuthOptions())
                .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
        } else {
            return this.http.post(this.apiUrl + "prices", price, this.createAuthOptions())
                .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
        }
    }

    public Delete(price: Price): Observable<any> {
        return this.http.delete(this.apiUrl + "prices/" + + price.priceId, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }
}
