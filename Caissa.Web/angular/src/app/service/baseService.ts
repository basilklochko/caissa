import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { environment } from "../../environments/environment";

export class BaseService {
    protected apiUrl = environment.apiUrl;

    constructor(protected http: Http) {
    }

    protected createAuthOptions(): RequestOptions {
        const options = new RequestOptions({});
        options.headers = new Headers();
        // TODO - add some token for auth

        return options;
    }
}
