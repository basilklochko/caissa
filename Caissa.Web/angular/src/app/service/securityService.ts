import { Injectable } from "@angular/core";
import { Http, Response, RequestOptions, Headers } from "@angular/http";
// tslint:disable-next-line:import-blacklist
import { Observable } from "rxjs/Rx";
import { BaseService } from "./baseService";
import { Security } from "../model/security";
import { PageRequest } from "../model/pageRequest";

@Injectable()
export class SecurityService extends BaseService {
    constructor(protected http: Http) {
        super(http);
    }

    public GetOne(security: Security): Observable<Security> {
        return this.http.get(this.apiUrl + "securities/get-one/" + security.securityId, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }

    public GetAll(request: PageRequest): Observable<any> {
        return this.http.post(this.apiUrl + "securities/get-all", request, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }

    public Save(security: Security): Observable<any> {
        if (security.securityId) {
            return this.http.put(this.apiUrl + "securities/" + security.securityId, security, this.createAuthOptions())
                .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
        } else {
            return this.http.post(this.apiUrl + "securities", security, this.createAuthOptions())
                .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
        }
    }

    public Delete(security: Security): Observable<any> {
        return this.http.delete(this.apiUrl + "securities/" + + security.securityId, this.createAuthOptions())
            .map((res: Response) => res.json()).catch((error: any) => Observable.throw(error.json().error || "Server error"));
    }
}
