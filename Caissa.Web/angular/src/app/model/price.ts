export class Price {
    public priceId: number;
    public securityId: number;
    public date: Date;
    public eodPrice: number;
}
