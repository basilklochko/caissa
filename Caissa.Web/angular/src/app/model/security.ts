export class Security {
    public securityId: number;
    public name: string;
    public isin: string;
    public country: string;
}
