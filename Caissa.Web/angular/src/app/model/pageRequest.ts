export class PageRequest {
    public currentPage: number;
    public pageSize: number;
    public total: number;
}
