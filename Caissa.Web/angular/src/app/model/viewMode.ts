export enum ViewMode {
    List,
    Add,
    Edit,
    Delete
}
