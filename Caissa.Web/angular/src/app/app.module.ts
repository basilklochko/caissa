import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRouting } from "./app.routing";
import { AppMainComponent } from "./component/app.main";
import { AppSecuritiesComponent } from "./component/app.securities";
import { SecurityService } from "./service/securityService";
import { UiService } from "./service/uiService";
import { AppPricesComponent } from "./component/app.prices";
import { PriceService } from "./service/priceService";
import { AppPagerComponent } from "./component/app.pager";

@NgModule({
  declarations: [
    AppMainComponent, AppSecuritiesComponent, AppPricesComponent, AppPagerComponent
  ],
  imports: [BrowserModule, HttpModule, FormsModule, AppRouting],
  providers: [UiService, SecurityService, PriceService],
  bootstrap: [AppMainComponent]
})

export class AppModule { }
