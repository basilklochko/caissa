# Caissa Test #

### Technology Stack ###

* MS SQL
* .Net Core 2.1 
* TypeScript
* Angular / Angular CLI
* Bootstrap

### Configuration ###

* Install .Net Core if not present in the system 
* Install node and npm if not present in the system
* Create database using the db.sql script in the Caissa.Db project
* Provide proper connection string for the CaissaDatabase element of the appsettings.json file in the Caissa.Api project
* Provide CORs data for the AllowCors element of the appsettings.json file in the Caissa.Api project
* Provide proper urls for the apiUrl element of the environment files under the Class.Web/angular/src/environments folder
* Provide proper port for the web application in the deploy-web file (argument, defaulted to 5000)
* Provide proper port for the api application in the deploy-api file (argument, defaulted to 5005)

### Installation ###

1. Execute the build-client file, let it finish before the next steps
2. Launch the deploy-api file
3. Launch the deploy-web file
