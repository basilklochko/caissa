﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Caissa.Api
{
    public class Request
    {
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
