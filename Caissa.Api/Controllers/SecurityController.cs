﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caissa.Api.Response;
using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Caissa.Api.Controllers
{
    [EnableCors("AllowWeb")]
    [Route("api/securities")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        const string error = "Error occurred, please try again";
        private readonly ISecurityRepository securityRepository;

        public SecurityController(ISecurityRepository securityRepository)
        {
            this.securityRepository = securityRepository;
        }

       // POST api/securities/get-all
       [HttpPost]
       [Route("get-all")]
        public async Task<Response<IEnumerable<Security>>> GetAll(Request request)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<IEnumerable<Security>>();

                try
                {
                    var result = securityRepository.Get(request.CurrentPage, request.PageSize);
                    response.Total = result.Total;
                    response.Data = result.Data;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // GET api/securities/get-one/5
        [HttpGet]
        [Route("get-one/{id}")]
        public async Task<Response<Security>> GetOne(long id)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<Security>();

                try
                {
                    var result = securityRepository.Get(id);
                    response.Data = result;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // POST api/securities
        [HttpPost]
        public async Task<Response<bool>> Post(Security security)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    var result = securityRepository.Add(security);
                    response.Data = result.SecurityId > 0;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // PUT api/securities/5
        [HttpPut("{id}")]
        public async Task<Response<bool>> Put(long id, Security security)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    var result = securityRepository.Edit(security);
                    response.Data = result.SecurityId > 0;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // DELETE api/securities/5
        [HttpDelete("{id}")]
        public async Task<Response<bool>> Delete(long id)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    var result = securityRepository.Delete(id);
                    response.Data = result;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }
    }
}
