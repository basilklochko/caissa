﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caissa.Api.Response;
using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Caissa.Api.Controllers
{
    [EnableCors("AllowWeb")]
    [Route("api/prices")]
    [ApiController]
    public class PriceController : ControllerBase
    {
        const string error = "Error occurred, please try again";
        private readonly IPriceRepository priceRepository;

        public PriceController(IPriceRepository priceRepository)
        {
            this.priceRepository = priceRepository;
        }

        // POST api/prices/get-all/5
        [HttpPost]
        [Route("get-all/{id}")]
        public async Task<Response<IEnumerable<Price>>> Post(int id, Request request)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<IEnumerable<Price>>();

                try
                {
                    var result = priceRepository.GetBySecurityId(id, request.CurrentPage, request.PageSize);
                    response.Total = result.Total;
                    response.Data = result.Data;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // GET api/prices/get-one/5
        [HttpGet]
        [Route("get-one/{id}")]
        public async Task<Response<Price>> GetOne(long id)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<Price>();

                try
                {
                    var result = priceRepository.Get(id);
                    response.Data = result;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        //// POST api/prices
        [HttpPost]
        public async Task<Response<bool>> Post(Price price)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    if (priceRepository.CheckIfDayExists(price))
                    {
                        response.Error = "Price for this day has been already logged";
                    }
                    else
                    {
                        var result = priceRepository.Add(price);
                        response.Data = result.PriceId > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }

        // PUT api/prices/5
        [HttpPut("{id}")]
        public async Task<Response<bool>> Put(int id, Price price)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    if (priceRepository.CheckIfDayExists(price))
                    {
                        response.Error = "Price for this day has been already logged";
                    }
                    else
                    {
                        var result = priceRepository.Edit(price);
                        response.Data = result.PriceId > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = $"{ex.Message}";
                }

                return response;
            });

            return await task;
        }

        // DELETE api/prices/5
        [HttpDelete("{id}")]
        public async Task<Response<bool>> Delete(long id)
        {
            var task = Task.Run(() =>
            {
                var response = new Response<bool>()
                {
                    Data = false
                };

                try
                {
                    var result = priceRepository.Delete(id);
                    response.Data = result;
                }
                catch (Exception ex)
                {
                    // Log ex
                    response.Error = error;
                }

                return response;
            });

            return await task;
        }
    }
}
