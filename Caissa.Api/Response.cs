﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Caissa.Api.Response
{
    public class Response<T>
    {
        public T Data { get; set; }

        public string Error { get; set; }

        public int Total{ get; set; }
    }
}
