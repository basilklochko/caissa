﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caissa.Repository.Implementation;
using Caissa.Repository.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Caissa.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddJsonFile("appsettings.json").Build();

            services.AddTransient<ISecurityRepository>(s => new SecurityRepository(configuration.GetConnectionString("CaissaDatabase")));
            services.AddTransient<IPriceRepository>(s => new PriceRepository(configuration.GetConnectionString("CaissaDatabase")));

            var origins = new List<string>();

            foreach (var section in configuration.GetChildren())
            {
                if (section.Key == "AllowCors")
                {
                    origins = section.Value.Split(new char[] { ',' }).ToList();
                    //origins = section.Value;
                }
            }

            if (!origins.Any())
            {
                throw new Exception("No Cors configuration found!");
            }

            services.AddCors(options => options.AddPolicy("AllowWeb", p => p.WithOrigins(origins.ToArray()).AllowAnyMethod().AllowAnyHeader()));
            //services.AddCors(options => options.AddPolicy("AllowWeb", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowWeb");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}