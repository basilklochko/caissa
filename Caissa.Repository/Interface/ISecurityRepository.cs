﻿using Caissa.Repository.Model;
using System.Collections.Generic;

namespace Caissa.Repository.Interface
{
    public interface ISecurityRepository
    {
        PageOfData<List<Security>> Get(int currentPage, int pageSize);

        Security Get(long securityId);

        Security Add(Security security);

        Security Edit(Security security);

        bool Delete(long securityId);
    }
}
