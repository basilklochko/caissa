﻿using Caissa.Repository.Model;
using System.Collections.Generic;

namespace Caissa.Repository.Interface
{
    public interface IPriceRepository
    {
        PageOfData<List<Price>> GetBySecurityId(long securityId, int currentPage, int pageSize);

        Price Get(long priceId);

        Price Add(Price price);

        Price Edit(Price price);

        bool Delete(long securityId);

        bool CheckIfDayExists(Price price);
    }
}
