﻿using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using System.Collections.Generic;
using System.Linq;
using Security = Caissa.Repository.Model.Security;

namespace Caissa.Repository.Implementation
{
    public class SecurityRepository : RepositoryBase, ISecurityRepository
    {
        public SecurityRepository(string connectionString) : base(connectionString)
        {

        }

        public Security Add(Security security)
        {
            var data = SecurityToDb(security);
            context.Security.Add(data);
            context.SaveChanges();

            return SecurityToModel(data);
        }

        public bool Delete(long securityId)
        {
            var data = context.Security.FirstOrDefault(x => x.SecurityId == securityId);

            if (data == null)
            {
                return false;
            }

            context.Security.Remove(data);
            context.SaveChanges();

            return true;
        }

        public Security Edit(Security security)
        {
            var data = context.Security.FirstOrDefault(x => x.SecurityId == security.SecurityId);

            if (data != null)
            {
                data.Name = security.Name;
                data.ISIN = security.ISIN;
                data.Country = security.Country;
                context.SaveChanges();
            }

            return SecurityToModel(data);
        }

        public PageOfData<List<Security>> Get(int currentPage, int pageSize)
        {
            var query = context.Security.OrderByDescending(x => x.SecurityId);

            return new PageOfData<List<Security>>()
            {
                Total = query.Count(),
                Data = query.Skip(currentPage * pageSize).Take(pageSize).Select(x => SecurityToModel(x)).ToList()
            };
        }

        public Security Get(long securityId)
        {
            var result = new Security();
            var data = context.Security.FirstOrDefault(x => x.SecurityId == securityId);

            if (data != null)
            {
                result = SecurityToModel(data);
            }

            return result;
        }
    }
}
