﻿using Caissa.Db;
using Caissa.Repository.Interface;
using Caissa.Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Price = Caissa.Repository.Model.Price;

namespace Caissa.Repository.Implementation
{
    public class PriceRepository : RepositoryBase, IPriceRepository
    {
        public PriceRepository(string connectionString) : base(connectionString)
        {

        }

        public Price Add(Price price)
        {
            var data = PriceToDb(price);
            context.Price.Add(data);
            context.SaveChanges();

            return PriceToModel(data);
        }

        public bool Delete(long priceId)
        {
            var data = context.Price.FirstOrDefault(x => x.PriceId == priceId);

            if (data == null)
            {
                return false;
            }

            context.Price.Remove(data);
            context.SaveChanges();

            return true;
        }

        public Price Edit(Price price)
        {
            var data = context.Price.FirstOrDefault(x => x.PriceId == price.PriceId);

            if (data != null)
            {
                data.Date = price.Date;
                data.EODPrice = price.EODPrice;
                context.SaveChanges();
            }

            return PriceToModel(data);
        }

        public Price Get(long priceId)
        {
            var result = new Price();
            var data = context.Price.FirstOrDefault(x => x.PriceId == priceId);

            if (data != null)
            {
                result = PriceToModel(data);
            }

            return result;
        }

        public PageOfData<List<Price>> GetBySecurityId(long securityId, int currentPage, int pageSize)
        {
            var query = context.Price.Where(x => x.SecurityId == securityId).OrderByDescending(x => x.Date);

            return new PageOfData<List<Price>>()
            {
                Total = query.Count(),
                Data = query.Skip(currentPage * pageSize).Take(pageSize).Select(x => PriceToModel(x)).ToList()
            };
        }

        public bool CheckIfDayExists(Price price)
        {
            var result = false;

            if (price.PriceId == 0)
            {
                result = context.Price.Any(x => x.SecurityId == price.SecurityId && x.Date == price.Date);
            }
            else
            {
                result = context.Price.Any(x => x.SecurityId == price.SecurityId && x.Date == price.Date && x.PriceId != price.PriceId);
            }

            return result;
        }
    }
}
