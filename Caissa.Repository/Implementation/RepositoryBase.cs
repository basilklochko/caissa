﻿using Caissa.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caissa.Repository.Implementation
{
    public abstract class RepositoryBase
    {
        public RepositoryBase(string connectionString)
        {
            context = new CaissaContext(connectionString);
        }

        private string ConnectionString { get; set; }

        protected readonly CaissaContext context;

        protected Db.Security SecurityToDb(Model.Security security)
        {
            return new Db.Security()
            {
                SecurityId = security.SecurityId,
                Name = security.Name,
                ISIN = security.ISIN,
                Country = security.Country
            };
        }

        protected Model.Security SecurityToModel(Db.Security security)
        {
            return new Model.Security()
            {
                SecurityId = security.SecurityId,
                Name = security.Name,
                ISIN = security.ISIN,
                Country = security.Country
            };
        }

        protected Db.Price PriceToDb(Model.Price price)
        {
            return new Db.Price()
            {
                PriceId = price.PriceId,
                SecurityId = price.SecurityId,
                Date = price.Date,
                EODPrice = price.EODPrice
            };
        }

        protected Model.Price PriceToModel(Db.Price price)
        {
            return new Model.Price()
            {
                PriceId = price.PriceId,
                SecurityId = price.SecurityId,
                Date = price.Date,
                EODPrice = price.EODPrice
            };
        }
    }
}
