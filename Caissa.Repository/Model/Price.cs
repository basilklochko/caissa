﻿using System;

namespace Caissa.Repository.Model
{
    public class Price
    {
        public long PriceId { get; set; }

        public long SecurityId { get; set; }

        public DateTime Date { get; set; }

        public double EODPrice { get; set; }
    }
}
