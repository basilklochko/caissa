﻿namespace Caissa.Repository.Model
{
    public class PageOfData<T>
    {
        public T Data { get; set; }

        public int Total { get; set; }
    }
}
