﻿namespace Caissa.Repository.Model
{
    public class Security
    {
        public long SecurityId { get; set; }

        public string Name { get; set; }

        public string ISIN { get; set; }

        public string Country { get; set; }
    }
}
