USE [master]
GO
/****** Object:  Database [Caissa]    Script Date: 3/3/2019 12:50:13 PM ******/
CREATE DATABASE [Caissa]
 GO
USE [Caissa]
GO
/****** Object:  Table [dbo].[Price]    Script Date: 3/3/2019 12:50:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price](
	[PriceId] [bigint] IDENTITY(1,1) NOT NULL,
	[SecurityId] [bigint] NOT NULL,
	[Date] [date] NOT NULL,
	[EODPrice] [float] NOT NULL,
 CONSTRAINT [PK_Price] PRIMARY KEY CLUSTERED 
(
	[PriceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Security]    Script Date: 3/3/2019 12:50:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Security](
	[SecurityId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[ISIN] [varchar](20) NOT NULL,
	[Country] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Security] PRIMARY KEY CLUSTERED 
(
	[SecurityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Price]  WITH CHECK ADD  CONSTRAINT [FK_Price_Security] FOREIGN KEY([SecurityId])
REFERENCES [dbo].[Security] ([SecurityId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Price] CHECK CONSTRAINT [FK_Price_Security]
GO
USE [master]
GO
ALTER DATABASE [Caissa] SET  READ_WRITE 
GO
