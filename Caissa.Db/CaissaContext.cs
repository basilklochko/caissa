﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Caissa.Db
{
    public class CaissaContext : DbContext
    {
        public CaissaContext(DbContextOptions<CaissaContext> options) : base(options)
        {
        }

        public CaissaContext(string connectionString) 
        {
            ConnectionString = connectionString;
        }

        private string ConnectionString { get; set; }

        public DbSet<Security> Security { get; set; }

        public DbSet<Price> Price { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }
    }

    public class Security
    {
        public long SecurityId { get; set; }

        public string Name { get; set; }

        public string ISIN { get; set; }

        public string Country { get; set; }
    }

    public class Price
    {
        public long PriceId { get; set; }

        public long SecurityId { get; set; }

        public DateTime Date { get; set; }

        public double EODPrice { get; set; }
    }
}
